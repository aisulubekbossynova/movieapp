//
//  SettingsConstant.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/20/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import Foundation
struct SettingsConstant {
    static let cellId = "cellId"
    static let footerId = "footerId"
    static let moviesURL = "https://api.themoviedb.org/3/"
    static let apiKey = "d4d382359cbd421e1e472d639fb182a5"
    static let contentType = "application/json;charset=utf-8"
    static let authorizationBearer = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkNGQzODIzNTljYmQ0MjFlMWU0NzJkNjM5ZmIxODJhNSIsInN1YiI6IjVmM2ZlZWVkYzE3NWIyMDAzNDVlYmExMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.XgFcZ2AyGFMzWTILBUdCfi7Qm4uu2_NMqWJuZ-Gp3gM"
}
