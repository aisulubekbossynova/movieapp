//
//  MovieBaseController.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/20/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import UIKit
import Moya
import RxSwift
class MovieBaseController: BaseListController, UICollectionViewDelegateFlowLayout {
    
    fileprivate var currentPageNumber = 1
    var results = [Results]()
    var favListArray: NSMutableArray = []
    
    fileprivate var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.object(forKey: "favList") != nil {
            favListArray = NSMutableArray.init(array: UserDefaults.standard.object(forKey: "favList") as! NSMutableArray)
        }
        self.results = []
        self.collectionView.backgroundColor = .black
        collectionView.register(MovieCell.self, forCellWithReuseIdentifier: SettingsConstant.cellId)
        collectionView.register(LoadingFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: SettingsConstant.footerId)
        pullRefresh()
        currentPageNumber = 1
        getMoviesList()
    }
    
    fileprivate func pullRefresh() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.collectionView!.refreshControl = refresher
        self.refresher.tintColor = UIColor.gray
        self.refresher.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    @objc func refreshData() {
        self.results = []
        self.collectionView!.refreshControl?.beginRefreshing()
        currentPageNumber = 1
        getMoviesList()
        stopRefresher()
    }
    
    func stopRefresher() {
        self.collectionView!.refreshControl?.endRefreshing()
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SettingsConstant.footerId, for: indexPath)
        return footer
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let height: CGFloat = isDonePaginating ? 0 : 100
        return .init(width: view.frame.width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    var isPaginating = false
    var isDonePaginating = false
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingsConstant.cellId, for: indexPath) as! MovieCell
        let movie = results[indexPath.item]
        cell.configure(with: movie)
        
        if favListArray.contains(cell.nameLabel.text!) {
            cell.favButton.setImage(UIImage(named: "star_selected")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }else{
            cell.favButton.setImage(UIImage(named: "star_black")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        cell.favButton.tag = indexPath.item
        cell.favButton.addTarget(self, action: #selector(addToFav), for: .touchUpInside)
        if indexPath.item == results.count - 1 && !isPaginating {
            print("fetch more data")
            isPaginating = true
            self.currentPageNumber += 1
            getMoviesList()
        }
        
        return cell
    }
    @objc func getMoviesList(){
        Service.shared.fetchGenericJSONData(pageNumber: self.currentPageNumber ) { (movieResult: MovieResponse?, err) in
            self.isPaginating = false
            if let err = err {
                print("Failed to paginate data:", err)
                self.results = []
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                self.collectionView.setEmptyMessage("К сожалению, сервис недоступен.")
                self.currentPageNumber = 1
                return
            }
            self.collectionView.restore()
            if movieResult?.results.count == 0 {
                self.isDonePaginating = true
            }
            self.results += movieResult?.results ?? []
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            if(self.results.count == 0){
                self.collectionView.setEmptyMessage("К сожалению, список фильмов недоступен.")
            }
        }
    }
    @objc func addToFav(sender: UIButton) {
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: sender.tag, section: 0)) as! MovieCell
        guard let name = cell.nameLabel.text else { return }
        if favListArray.contains(name) {
            favListArray.remove(name)
        }
        else{
            favListArray.add(name)
        }
        collectionView.reloadData()
        UserDefaults.standard.set(favListArray, forKey: "favList")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailMovieViewController()
        let result = results[indexPath.item]
        vc.result = result
        vc.favListArray = favListArray
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 20, height: 180)
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "ATF weekend movies"
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.prefersLargeTitles = true
        collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = nil
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    
}

