//
//  DetailMovieViewController.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/20/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import UIKit

class DetailMovieViewController: UIViewController {
    
    let viewModel: GenreViewModelProtocol = GenreResponse()
    
    var favListArray: NSMutableArray = []
    
    var result: Results! {
        didSet{
            detailView.genres = viewModel.genres
            detailView.result = result
        }
    }
    
    let detailView = DetailMovieView()
    
    lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "back_button")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.setTitleColor(button.tintColor, for: .normal)
        return button
    }()
    
    lazy var favButton: UIButton = {
        let b = UIButton(type: .system)
        b.setImage(UIImage(named: "star")?.withRenderingMode(.alwaysOriginal), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        setupSignals()
        
        view.addSubview(detailView)
        detailView.fillSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.object(forKey: "favList") != nil {
            guard let name = detailView.nameLabel.text else { return }
            if favListArray.contains(name) {
                favButton.setImage(UIImage(named: "star_selected")?.withRenderingMode(.alwaysOriginal), for: .normal)
            }else{
                favButton.setImage(UIImage(named: "star")?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
        }
    }
    
    fileprivate func setupSignals() {
        
        backButton.addTarget(self, action: #selector(closeFlow), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        favButton.addTarget(self, action: #selector(addToFav), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: favButton)
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        let orient = UIApplication.shared.statusBarOrientation
        DispatchQueue.main.async {
            switch orient {
            case .portrait:
                self.detailView.alloverStackView.axis = .horizontal
                break
            default:
                self.detailView.alloverStackView.axis = .vertical
                self.view.layoutIfNeeded()
                break
            }
        }
    }
    
    
    @objc private func closeFlow() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addToFav(sender: UIButton) {
        guard let name = detailView.nameLabel.text else { return }
        if favListArray.contains(name) {
            favListArray.remove(name)
            favButton.setImage(UIImage(named: "star")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        else{
            favListArray.add(name)
            favButton.setImage(UIImage(named: "star_selected")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        UserDefaults.standard.set(favListArray, forKey: "favList")
        
    }
    
}

extension DetailMovieViewController:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
