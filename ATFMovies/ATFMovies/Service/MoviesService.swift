//
//  MoviesService.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/20/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import Foundation
import Moya

enum MoviesService {
    case getMovies(page: Int)
}

extension MoviesService: TargetType {
    var baseURL: URL {
        switch self {
        case .getMovies:
            guard let url = URL(string: SettingsConstant.moviesURL) else { fatalError(NSLocalizedString("Base auth url is not configured", comment: "Base url")) }
            return url
        }
    }
    
    var path: String {
        switch self {
        case .getMovies:
            return "discover/movie"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getMovies:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .getMovies(let page):
            return .requestParameters(parameters: ["page": page], encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getMovies:
            return Data()
        }
    }
    
    var headers: [String: String]? {
        var httpHeaders = [String: String]()
        switch self {
        case .getMovies:
            httpHeaders["api_key"] = SettingsConstant.apiKey
            httpHeaders["Content-Type"] = SettingsConstant.contentType
            httpHeaders["Authorization"] = SettingsConstant.authorizationBearer
            break
        }
        return httpHeaders
    }
}

