//
//  Service.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/20/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Realm
import RealmSwift
class Service {
    static let shared = Service()
    let moviesProvider = MoyaProvider<MoviesService>()
    
    func fetchGenericJSONData<MovieResponse: Decodable>(pageNumber: Int, completion: @escaping (MovieResponse?, Error?) -> () ){
        moviesProvider.request(MoviesService.getMovies(page: pageNumber)) { result in
        switch result {
        case let .success(resultResponse):
            print("result response \(resultResponse.statusCode)")
                do {
                    let objects = try JSONDecoder().decode(MovieResponse.self, from: resultResponse.data)
                    print("result response \(objects)")
                    completion(objects,nil)
                } catch {
                    completion(nil, error)
                    print("Failed to decode:",error)
                }
            break
        case let .failure(error):
            completion(nil, error)
            break
        }
    }
    
    }
    
    
    
}
