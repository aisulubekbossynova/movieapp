//
//  GenreResponse.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/21/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import Foundation
protocol GenreViewModelProtocol: class {
    var genres: [Genres] { get }
}

class GenreResponse: GenreViewModelProtocol {
    var genres = [Genres]()
    
    init() {
        retrieveGenres()
    }
    
    func retrieveGenres() {
        guard let url = Bundle.main.url(forResource: "genre", withExtension: "json") else { return }
        do {
            let data = try Data(contentsOf: url)
            genres = try JSONDecoder().decode(GenresResponse.self, from: data).genres
            
        } catch {
            debugPrint(error.localizedDescription)
        }
    }
    
}

struct GenresResponse: Decodable {
    let genres: [Genres]
}

struct Genres: Decodable {
    let id: Int
    let name: String
}
