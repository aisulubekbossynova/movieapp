//
//  StackViewHelper.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/21/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import Foundation
import UIKit
extension UIStackView {
    convenience init(arrangedSubviews: [UIView]? = nil, axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution, spacing: CGFloat) {
        if let arrangedSubviews = arrangedSubviews {
            self.init(arrangedSubviews: arrangedSubviews)
        } else {
            self.init()
        }
        (self.axis, self.spacing, self.distribution) = (axis, spacing, distribution)
    }
}
