//
//  CollectionViewHelper.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/22/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import Foundation
import UIKit
extension UICollectionView {
       func setEmptyMessage(_ message: String) {
         let emptyCartView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height - 25))
         emptyCartView.sizeToFit()
         self.backgroundView = emptyCartView
         emptyCartView.backgroundColor = .black
         let welcomeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: emptyCartView.bounds.size.width - 40, height: 20))
         welcomeLabel.text = ""
         welcomeLabel.textColor = .black
         welcomeLabel.numberOfLines = 0;
         welcomeLabel.textAlignment = .center;
         welcomeLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
         welcomeLabel.sizeToFit()
         welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
         emptyCartView.addSubview(welcomeLabel)
         welcomeLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
         welcomeLabel.widthAnchor.constraint(equalToConstant: emptyCartView.bounds.size.width - 40).isActive = true
         welcomeLabel.centerXAnchor.constraint(equalTo: emptyCartView.centerXAnchor).isActive = true
         welcomeLabel.centerYAnchor.constraint(equalTo: emptyCartView.centerYAnchor, constant: -170).isActive = true
         
         let imageBG = UIImageView(image : UIImage(named : "internet_error")!)
         emptyCartView.addSubview(imageBG)
         imageBG.translatesAutoresizingMaskIntoConstraints = false
         imageBG.widthAnchor.constraint(equalToConstant: 300).isActive = true
         imageBG.heightAnchor.constraint(equalToConstant: 300).isActive = true
         imageBG.centerXAnchor.constraint(equalTo: emptyCartView.centerXAnchor).isActive = true
         imageBG.centerYAnchor.constraint(equalTo: emptyCartView.centerYAnchor, constant: -120).isActive = true
         let containerView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width - 40, height: 270))
         containerView.sizeToFit()
         containerView.center = CGPoint(x: emptyCartView.frame.size.width / 2, y: emptyCartView.frame.size.height / 2 + 30)
         containerView.layer.masksToBounds = true
         containerView.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin]
         
         let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: containerView.bounds.size.width - 40, height: containerView.bounds.size.height ))
         messageLabel.text = message
         messageLabel.textColor = .white
         messageLabel.numberOfLines = 0;
         messageLabel.textAlignment = .center;
         messageLabel.font = UIFont(name: "TrebuchetMS", size: 16)
         messageLabel.sizeToFit()
         messageLabel.center = CGPoint(x: containerView.frame.size.width / 2, y: (containerView.frame.size.height / 2 + 65))
         containerView.addSubview(messageLabel)
         
         emptyCartView.addSubview(containerView)
         
     }
     func restore() {
           self.backgroundView = nil
       }
    
}
