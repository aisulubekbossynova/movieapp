//
//  DetailMovieView.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/21/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import UIKit
import SDWebImage

class DetailMovieView: UIView {
    
    var genres: [Genres]!
    var genreNames = [String]()
    
    var result: Results! {
        didSet {
            nameLabel.text = result.title
            originalTitleLabel.text = result.originTitle
            rateLabel.text = "Rating: \(result.voteAverage)/10"
            releaseDateLabel.text = "Release: " + (result.releaseDate ?? "Undefined")
            langLabel.text = "Language: " + (result.originLan ?? "ru")
            voteLabel.text = "Votes: \(result.voteCount)"
            
            for genre in genres {
                for id in result.genreId {
                    if id == genre.id {
                        genreNames.append(genre.name)
                        break
                    }
                }
            }
            
            genreLabel.text = "Genre: \(genreNames.joined(separator: " • "))"
            aboutTextLabel.text = result.overview
            
            let imageUrl = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/"
            
            if let poster = result.posterPath {
                posterImageView.sd_setImage(with: URL(string: imageUrl + poster))
            }
            
            guard let backdrop = result.backdropPath else { return }
            backdropImageView.sd_setImage(with: URL(string: imageUrl + backdrop))
        }
    }
    
    lazy var alloverStackView = UIStackView()
    
    lazy var backdropImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.image = UIImage(named: "backdrop")?.withRenderingMode(.alwaysOriginal)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        return iv
    }()
    
    let shadowView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.8)
        return v
    }()
    
    lazy var posterImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.image = UIImage(named: "ww")?.withRenderingMode(.alwaysOriginal)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 20
        return iv
    }()
    
    lazy var nameLabel: UILabel = {
        let l = UILabel()
        l.text = "Камуфляж и шпионаж"
        l.textColor = .white
        l.textAlignment = .center
        l.font = .boldSystemFont(ofSize: 23)
        l.numberOfLines = 0
        return l
    }()
    
    lazy var originalTitleLabel: UILabel = {
        let l = UILabel()
        l.text = "Spies in Disguise"
        l.font = .systemFont(ofSize: 14, weight: .medium)
        l.textAlignment = .center
        l.textColor = .lightGray
        l.translatesAutoresizingMaskIntoConstraints = false
        l.numberOfLines = 0
        return l
    }()
    
    lazy var rateLabel: UILabel = {
        let l = UILabel()
        l.text = "Rating: 7.0/10"
        l.font = .systemFont(ofSize: 14, weight: .medium)
        l.textAlignment = .left
        l.textColor = .lightGray
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var releaseDateLabel: UILabel = {
        let l = UILabel()
        l.text = "Release Date: 2019-12-24"
        l.font = .systemFont(ofSize: 14, weight: .medium)
        l.textAlignment = .left
        l.textColor = .lightGray
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var langLabel: UILabel = {
        let l = UILabel()
        l.text = "Language: en"
        l.font = .systemFont(ofSize: 14, weight: .medium)
        l.textAlignment = .left
        l.textColor = .lightGray
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var voteLabel: UILabel = {
        let l = UILabel()
        l.text = "Votes: 368"
        l.font = .systemFont(ofSize: 14, weight: .medium)
        l.textAlignment = .left
        l.textColor = .lightGray
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var genreLabel: UILabel = {
        let l = UILabel()
        l.text = "Genre: Action • Adventure • Animation"
        l.font = .systemFont(ofSize: 14, weight: .medium)
        l.textAlignment = .left
        l.numberOfLines = 0
        l.textColor = .lightGray
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var aboutLabel: UILabel = {
        let l = UILabel()
        l.text = "Overview:"
        l.font = .boldSystemFont(ofSize: 20)
        l.textAlignment = .left
        l.textColor = .white
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var aboutTextLabel: UILabel = {
        let l = UILabel()
        l.text = "Primatologist Davis Okoye shares an unshakable bond with George, the extraordinarily intelligent, silverback gorilla who has been in his care since birth. But a rogue genetic experiment gone awry mutates this gentle ape into a raging creature of enormous size. To make matters worse, it’s soon discovered there are other similarly altered animals. As these newly created alpha predators tear across North America, destroying everything in their path, Okoye teams with a discredited genetic engineer to secure an antidote, fighting his way through an ever-changing battlefield, not only to halt a global catastrophe but to save the fearsome creature that was once his friend."
        l.textAlignment = .left
        l.font = .systemFont(ofSize: 15, weight: .light)
        l.textColor = .lightGray
        l.numberOfLines = 0
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    
    //MARK:- Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayouts()
    }
    
    
    fileprivate func setupLayouts() {
        backgroundColor = .white
        
        addSubview(backdropImageView)
        backdropImageView.fillSuperview()
        backdropImageView.addSubview(shadowView)
        shadowView.fillSuperview()
        
        //MARK:- topContainer
        let topContainerView = UIView()
        topContainerView.backgroundColor = .clear
        topContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        
        let infoStackView = UIStackView(arrangedSubviews: [UIView(),
                                                           rateLabel,
                                                           releaseDateLabel,
                                                           langLabel,
                                                           voteLabel,
                                                           genreLabel,
                                                           UIView()],
                                        axis: .vertical,
                                        distribution: .equalSpacing,
                                        spacing: 10)
        
        let stackView = UIStackView(arrangedSubviews: [UIStackView(arrangedSubviews: [nameLabel,
                                                                                      originalTitleLabel],
                                                                   axis: .vertical,
                                                                   distribution: .fillProportionally,
                                                                   spacing: 10),
                                                       UIStackView(arrangedSubviews:[posterImageView,
                                                                                      infoStackView],
                                                                   axis: .horizontal,
                                                                   distribution: .fillEqually,
                                                                   spacing: 15)],
                                    axis: .vertical,
                                    distribution: .equalSpacing,
                                    spacing: 15)
        
        topContainerView.addSubview(stackView)
        stackView.fillSuperview(padding: .init(top: 0, left: 20, bottom: 0, right: 20))
        
        //MARK:- bottomContainer
        
        let bottomContainerView = UIView()
        bottomContainerView.backgroundColor = .clear
        bottomContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        bottomContainerView.addSubview(aboutLabel)
        aboutLabel.anchor(top: bottomContainerView.topAnchor, leading: bottomContainerView.leadingAnchor, bottom: nil, trailing: bottomContainerView.trailingAnchor, padding: .init(top: 30, left: 20, bottom: 0, right: 20))
        
        bottomContainerView.addSubview(aboutTextLabel)
        aboutTextLabel.anchor(top: aboutLabel.bottomAnchor, leading: aboutLabel.leadingAnchor, bottom: nil, trailing: aboutLabel.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        
        //MARK:- alloverStackView
        alloverStackView = UIStackView(arrangedSubviews: [topContainerView, bottomContainerView], axis: .vertical, distribution: .fillEqually, spacing: 0)
        alloverStackView.distribution = .fillEqually
        alloverStackView.axis = .vertical
        
        
        shadowView.addSubview(alloverStackView)
        alloverStackView.anchor(top: safeAreaLayoutGuide.topAnchor, leading: safeAreaLayoutGuide.leadingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, trailing: safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
