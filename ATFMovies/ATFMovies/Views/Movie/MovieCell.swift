//
//  MovieCell.swift
//  ATFMovies
//
//  Created by aisulubekbossynova on 8/21/20.
//  Copyright © 2020 aisulubekbossynova. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCell: UICollectionViewCell {
    
    
    var movie: Results!
    
    let bView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 10
        v.layer.addRoundedShadow()
        return v
    }()
    
    lazy var posterImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.image = UIImage(named: "ww")?.withRenderingMode(.alwaysOriginal)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.layer.addShadow()
        return iv
    }()
    
    lazy var rateLabel: UILabel = {
        let l = UILabel()
        l.text = "7.0"
        l.font = .boldSystemFont(ofSize: 18)
        l.textAlignment = .center
        l.textColor = .white
        l.backgroundColor = UIColor(red: 0, green: 0.82, blue: 0.47, alpha: 1)
        l.layer.cornerRadius = 20
        l.clipsToBounds = true
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var nameLabel: UILabel = {
        let l = UILabel()
        l.text = "Movie Name"
        l.textColor = .darkGray
        l.textAlignment = .left
        l.font = .boldSystemFont(ofSize: 19)
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var aboutLabel: UILabel = {
        let l = UILabel()
        l.text = "Label test text"
        l.textAlignment = .left
        l.font = .systemFont(ofSize: 14, weight: .light)
        l.textColor = .lightGray
        l.numberOfLines = 3
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    
    lazy var favButton: UIButton = {
        let b = UIButton(type: .system)
        b.setImage(UIImage(named: "star_black")?.withRenderingMode(.alwaysOriginal), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
   

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayouts()
    }
    func configure(with movie: Results) {
        self.movie = movie
        nameLabel.text = movie.title
        aboutLabel.text = movie.overview
        rateLabel.text = String(movie.voteAverage)
        
        guard let poster = movie.posterPath else { return }
        
        let imageUrl = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/"
        
        posterImageView.sd_setImage(with: URL(string: imageUrl + poster))
    }
    
    fileprivate func setupLayouts() {
        addSubview(bView)
        bView.fillSuperview(padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        
        addSubview(posterImageView)
        posterImageView.anchor(top: nil, leading: bView.leadingAnchor, bottom: bView.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 10, bottom: 20, right: 0), size: .init(width: 120, height: 160))
        
        addSubview(rateLabel)
        rateLabel.anchor(top: nil, leading: posterImageView.trailingAnchor, bottom: posterImageView.bottomAnchor, trailing: posterImageView.trailingAnchor, padding: .init(top: 0, left: -20, bottom: 3, right: -20), size: .init(width: 40, height: 40))
        
        
        bView.addSubview(nameLabel)
        nameLabel.anchor(top: bView.topAnchor, leading: posterImageView.trailingAnchor, bottom: nil, trailing: bView.trailingAnchor, padding: .init(top: 5, left: 10, bottom: 0, right: 10))
        
        bView.addSubview(aboutLabel)
        aboutLabel.anchor(top: nameLabel.bottomAnchor, leading: nameLabel.leadingAnchor, bottom: nil, trailing: nameLabel.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        
        bView.addSubview(favButton)
        favButton.anchor(top: aboutLabel.bottomAnchor, leading: nil, bottom: bView.bottomAnchor, trailing: bView.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 10, right: 10), size: .init(width: 30, height: 30))
    }
   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
